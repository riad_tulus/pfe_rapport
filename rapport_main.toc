\select@language {french}
\contentsline {chapter}{R\IeC {\'e}sum\IeC {\'e}}{iii}{chapter*.1}
\contentsline {chapter}{Abstract}{iv}{chapter*.2}
\contentsline {chapter}{Liste des tableaux}{xi}{chapter*.4}
\contentsline {chapter}{Table des figures}{xiv}{chapter*.5}
\contentsline {chapter}{Abr\IeC {\'e}viations}{xvi}{chapter*.6}
\contentsline {chapter}{INTRODUCTION GENERALE}{1}{chapter*.7}
\contentsline {part}{I\hspace {1em}SYNTHESE BIBLIOGRAPHIQUE}{6}{part.1}
\contentsline {xpart}{SYNTHESE BIBLIOGRAPHIQUE}{7}{part.1}
\contentsline {chapter}{\numberline {1}G\IeC {\'e}n\IeC {\'e}ralit\IeC {\'e} sur le commerce \IeC {\'e}lectronique}{8}{chapter.1.1}
\contentsline {section}{\numberline {1.1}Commerce \IeC {\'E}lectronique}{9}{section.1.1.1}
\contentsline {subsection}{\numberline {1.1.1}Vari\IeC {\'e}t\IeC {\'e} des d\IeC {\'e}finitions}{9}{subsection.1.1.1.1}
\contentsline {subsection}{\numberline {1.1.2}Synth\IeC {\`e}se}{9}{subsection.1.1.1.2}
\contentsline {section}{\numberline {1.2}Typologie de l'e-commerce}{10}{section.1.1.2}
\contentsline {subsection}{\numberline {1.2.1}Governmenet to Business}{10}{subsection.1.1.2.1}
\contentsline {subsection}{\numberline {1.2.2}Governmenet to Customer}{10}{subsection.1.1.2.2}
\contentsline {subsection}{\numberline {1.2.3}Business to Customer}{11}{subsection.1.1.2.3}
\contentsline {subsection}{\numberline {1.2.4}Business to Business}{11}{subsection.1.1.2.4}
\contentsline {subsection}{\numberline {1.2.5}Customer to Customer}{11}{subsection.1.1.2.5}
\contentsline {section}{\numberline {1.3}\IeC {\'E}volution Historique}{12}{section.1.1.3}
\contentsline {subsection}{\numberline {1.3.1}L'\IeC {\'e}change de donn\IeC {\'e}es informatis\IeC {\'e}es (EDI) }{12}{subsection.1.1.3.1}
\contentsline {subsection}{\numberline {1.3.2}l'apparition d'internet}{12}{subsection.1.1.3.2}
\contentsline {subsection}{\numberline {1.3.3}Les principales dates}{13}{subsection.1.1.3.3}
\contentsline {section}{\numberline {1.4}Les facteurs du d\IeC {\'e}veloppement de l'e-commerce}{14}{section.1.1.4}
\contentsline {subsection}{\numberline {1.4.1}L'environnement technologique}{14}{subsection.1.1.4.1}
\contentsline {subsection}{\numberline {1.4.2}L'environnement financier}{14}{subsection.1.1.4.2}
\contentsline {subsection}{\numberline {1.4.3}L'environnement juridique et fiscal}{15}{subsection.1.1.4.3}
\contentsline {section}{\numberline {1.5}L'\IeC {\'e}cosyst\IeC {\`e}me du commerce \IeC {\'e}lectronique}{15}{section.1.1.5}
\contentsline {subsection}{\numberline {1.5.1}Le Processus du commerce \IeC {\'e}lectronique}{15}{subsection.1.1.5.1}
\contentsline {subsection}{\numberline {1.5.2}les intervenants du processus}{16}{subsection.1.1.5.2}
\contentsline {subsection}{\numberline {1.5.3}Sch\IeC {\'e}ma global du processus}{18}{subsection.1.1.5.3}
\contentsline {section}{\numberline {1.6}Le paiement en ligne}{19}{section.1.1.6}
\contentsline {subsection}{\numberline {1.6.1}les moyens de paiements en ligne}{20}{subsection.1.1.6.1}
\contentsline {subsubsection}{\numberline {1.6.1.1}paiement par carte bancaire}{20}{subsubsection.1.1.6.1.1}
\contentsline {subsubsection}{\numberline {1.6.1.2}paiement par virement bancaire en ligne}{21}{subsubsection.1.1.6.1.2}
\contentsline {subsubsection}{\numberline {1.6.1.3}eWallet (portefeuilles \IeC {\'e}lectroniques)}{21}{subsubsection.1.1.6.1.3}
\contentsline {subsubsection}{\numberline {1.6.1.4}Paiements par pr\IeC {\'e}l\IeC {\`e}vement automatique}{21}{subsubsection.1.1.6.1.4}
\contentsline {subsubsection}{\numberline {1.6.1.5}Paiements crypto-monnaie}{22}{subsubsection.1.1.6.1.5}
\contentsline {subsection}{\numberline {1.6.2}le choix du moyen de paiements}{22}{subsection.1.1.6.2}
\contentsline {section}{\numberline {1.7}Le march\IeC {\'e} mondial}{24}{section.1.1.7}
\contentsline {chapter}{\numberline {2}La s\IeC {\'e}curit\IeC {\'e} des solutions E-commerce}{27}{chapter.1.2}
\contentsline {section}{\numberline {2.1}Exigences fondamentales}{28}{section.1.2.1}
\contentsline {subsection}{\numberline {2.1.1}La disponibilit\IeC {\'e}}{28}{subsection.1.2.1.1}
\contentsline {subsection}{\numberline {2.1.2}La Confidentialit\IeC {\'e}}{28}{subsection.1.2.1.2}
\contentsline {subsection}{\numberline {2.1.3}L'int\IeC {\'e}grit\IeC {\'e}}{29}{subsection.1.2.1.3}
\contentsline {subsection}{\numberline {2.1.4}la non-r\IeC {\'e}pudiation}{29}{subsection.1.2.1.4}
\contentsline {section}{\numberline {2.2}\IeC {\'E}tude des menaces et des attaques}{30}{section.1.2.2}
\contentsline {subsection}{\numberline {2.2.1}Attaque par d\IeC {\'e}ni de service}{30}{subsection.1.2.2.1}
\contentsline {subsection}{\numberline {2.2.2}Attaque par rejeu}{31}{subsection.1.2.2.2}
\contentsline {subsection}{\numberline {2.2.3}Attaque par manipulation de donn\IeC {\'e}es}{31}{subsection.1.2.2.3}
\contentsline {subsection}{\numberline {2.2.4}Attaque par d\IeC {\'e}tournement de protocoles}{31}{subsection.1.2.2.4}
\contentsline {subsection}{\numberline {2.2.5}Attaques sur les bases de donn\IeC {\'e}es}{32}{subsection.1.2.2.5}
\contentsline {section}{\numberline {2.3}\IeC {\'E}tablissement d'une politique de s\IeC {\'e}curit\IeC {\'e}}{32}{section.1.2.3}
\contentsline {subsection}{\numberline {2.3.1}Protection au niveau physique}{33}{subsection.1.2.3.1}
\contentsline {subsection}{\numberline {2.3.2}Protection au niveau du serveur}{33}{subsection.1.2.3.2}
\contentsline {subsection}{\numberline {2.3.3}Protection au niveau du r\IeC {\'e}seau}{33}{subsection.1.2.3.3}
\contentsline {subsection}{\numberline {2.3.4} Protection au niveau de l\IeC {\textquoteright }application}{33}{subsection.1.2.3.4}
\contentsline {subsection}{\numberline {2.3.5} Secure Sockets Layer (SSL)}{34}{subsection.1.2.3.5}
\contentsline {subsection}{\numberline {2.3.6}3D-Secure}{36}{subsection.1.2.3.6}
\contentsline {subsection}{\numberline {2.3.7}Protection contre l\IeC {\textquoteright }Injection de SQL}{36}{subsection.1.2.3.7}
\contentsline {part}{II\hspace {1em}ETUDE CONCEPTUELLE}{38}{part.2}
\contentsline {xpart}{ETUDE CONCEPTUELLE}{39}{part.2}
\contentsline {chapter}{\numberline {1}Description g\IeC {\'e}n\IeC {\'e}rale de la solution}{40}{chapter.2.1}
\contentsline {section}{\numberline {1.1}Introduction}{40}{section.2.1.1}
\contentsline {section}{\numberline {1.2}Description de la solution}{40}{section.2.1.2}
\contentsline {section}{\numberline {1.3}M\IeC {\'e}thodologie de travail}{42}{section.2.1.3}
\contentsline {subsection}{\numberline {1.3.1}L'Unified Process UP}{42}{subsection.2.1.3.1}
\contentsline {subsection}{\numberline {1.3.2}D\IeC {\'e}marche g\IeC {\'e}n\IeC {\'e}rale}{42}{subsection.2.1.3.2}
\contentsline {section}{\numberline {1.4}Introduction du BDD}{44}{section.2.1.4}
\contentsline {subsection}{\numberline {1.4.1}Behavior Driven Development}{44}{subsection.2.1.4.1}
\contentsline {subsubsection}{\numberline {1.4.1.1}Les user stories}{44}{subsubsection.2.1.4.1.1}
\contentsline {subsection}{\numberline {1.4.2}l'application r\IeC {\'e}elle du BDD}{45}{subsection.2.1.4.2}
\contentsline {section}{\numberline {1.5}Conclusion}{46}{section.2.1.5}
\contentsline {chapter}{\numberline {2}RECUEIL DES BESOINS}{47}{chapter.2.2}
\contentsline {section}{\numberline {2.1}Introduction}{47}{section.2.2.1}
\contentsline {section}{\numberline {2.2}Identification des acteurs}{47}{section.2.2.2}
\contentsline {section}{\numberline {2.3}Identification et classification des modules et user stories}{48}{section.2.2.3}
\contentsline {subsection}{\numberline {2.3.1}Module du vendeur}{48}{subsection.2.2.3.1}
\contentsline {subsection}{\numberline {2.3.2}Module de l'acheteur}{51}{subsection.2.2.3.2}
\contentsline {subsection}{\numberline {2.3.3}Module du livreur}{52}{subsection.2.2.3.3}
\contentsline {subsection}{\numberline {2.3.4}Module de paiement}{54}{subsection.2.2.3.4}
\contentsline {subsection}{\numberline {2.3.5}Module de l'administrateur}{56}{subsection.2.2.3.5}
\contentsline {subsection}{\numberline {2.3.6}Exemple d'application de la m\IeC {\'e}thode BDD}{58}{subsection.2.2.3.6}
\contentsline {subsubsection}{\numberline {2.3.6.1}User Storie}{58}{subsubsection.2.2.3.6.1}
\contentsline {subsubsection}{\numberline {2.3.6.2}Sc\IeC {\'e}narios de test}{58}{subsubsection.2.2.3.6.2}
\contentsline {subsubsection}{\numberline {2.3.6.3}Automatisation des tests}{59}{subsubsection.2.2.3.6.3}
\contentsline {section}{\numberline {2.4}Conclusion}{60}{section.2.2.4}
\contentsline {chapter}{\numberline {3}ANALYSE ET CONCEPTION}{61}{chapter.2.3}
\contentsline {section}{\numberline {3.1}Introduction}{61}{section.2.3.1}
\contentsline {section}{\numberline {3.2}Pr\IeC {\'e}sentation de l'architecture de la solution}{61}{section.2.3.2}
\contentsline {section}{\numberline {3.3}D\IeC {\'e}coupage en package}{63}{section.2.3.3}
\contentsline {subsection}{\numberline {3.3.1}Package gestion du catalogue}{63}{subsection.2.3.3.1}
\contentsline {subsubsection}{\numberline {3.3.1.1}Processus d'ajout du produit}{63}{subsubsection.2.3.3.1.1}
\contentsline {subsection}{\numberline {3.3.2}Package gestion de restaurant}{67}{subsection.2.3.3.2}
\contentsline {subsubsection}{\numberline {3.3.2.1}Cr\IeC {\'e}ation de l'espace restaurant}{67}{subsubsection.2.3.3.2.1}
\contentsline {subsubsection}{\numberline {3.3.2.2}Cr\IeC {\'e}ation d'un emplacement}{67}{subsubsection.2.3.3.2.2}
\contentsline {subsection}{\numberline {3.3.3}Package gestion des commandes}{69}{subsection.2.3.3.3}
\contentsline {subsubsection}{\numberline {3.3.3.1}Passer une commande}{69}{subsubsection.2.3.3.3.1}
\contentsline {subsubsection}{\numberline {3.3.3.2}Traiter une commande}{71}{subsubsection.2.3.3.3.2}
\contentsline {subsection}{\numberline {3.3.4}Package gestion des utilisateurs}{73}{subsection.2.3.3.4}
\contentsline {subsubsection}{\numberline {3.3.4.1}Ajouter compte employ\IeC {\'e}e}{74}{subsubsection.2.3.3.4.1}
\contentsline {subsection}{\numberline {3.3.5}Package gestion du paiement}{74}{subsection.2.3.3.5}
\contentsline {section}{\numberline {3.4}Mod\IeC {\`e}le relationnel de la base de donn\IeC {\'e}es}{76}{section.2.3.4}
\contentsline {section}{\numberline {3.5}S\IeC {\'e}curit\IeC {\'e} de la marketplace}{77}{section.2.3.5}
\contentsline {subsection}{\numberline {3.5.1}Consommation des services par token}{77}{subsection.2.3.5.1}
\contentsline {subsection}{\numberline {3.5.2}Authentification SSL}{78}{subsection.2.3.5.2}
\contentsline {subsection}{\numberline {3.5.3}Pare-Feu}{79}{subsection.2.3.5.3}
\contentsline {section}{\numberline {3.6}Mod\IeC {\`e}le de d\IeC {\'e}ploiement}{80}{section.2.3.6}
\contentsline {section}{\numberline {3.7}Conclusion}{81}{section.2.3.7}
\contentsline {chapter}{\numberline {4}REALISATION DE LA SOLUTION}{82}{chapter.2.4}
\contentsline {section}{\numberline {4.1}Introduction}{82}{section.2.4.1}
\contentsline {section}{\numberline {4.2}Outils et environnmenet de d\IeC {\'e}veloppement}{82}{section.2.4.2}
\contentsline {subsection}{\numberline {4.2.1}Environnement de d\IeC {\'e}veloppement}{82}{subsection.2.4.2.1}
\contentsline {subsection}{\numberline {4.2.2}Outils de d\IeC {\'e}veloppement}{83}{subsection.2.4.2.2}
\contentsline {section}{\numberline {4.3}Architecture technique}{85}{section.2.4.3}
\contentsline {section}{\numberline {4.4}Int\IeC {\'e}gration de la solution de paiement PayPal}{85}{section.2.4.4}
\contentsline {subsection}{\numberline {4.4.1}Processus d'int\IeC {\'e}gration}{86}{subsection.2.4.4.1}
\contentsline {section}{\numberline {4.5}Configuration de la base de donn\IeC {\'e}es}{88}{section.2.4.5}
\contentsline {section}{\numberline {4.6}Test de la solution}{90}{section.2.4.6}
\contentsline {subsection}{\numberline {4.6.1}Test unitaires}{90}{subsection.2.4.6.1}
\contentsline {subsection}{\numberline {4.6.2}Test fonctionnels}{92}{subsection.2.4.6.2}
\contentsline {section}{\numberline {4.7}Pr\IeC {\'e}sentation de l'application}{93}{section.2.4.7}
\contentsline {subsection}{\numberline {4.7.1}Application de l'administrateur de la platforme}{93}{subsection.2.4.7.1}
\contentsline {subsection}{\numberline {4.7.2}la validation de la cr\IeC {\'e}ation d'un restaurant}{94}{subsection.2.4.7.2}
\contentsline {subsection}{\numberline {4.7.3}l'interface de la page d'accueil du propri\IeC {\'e}taire du restaurant}{94}{subsection.2.4.7.3}
\contentsline {subsection}{\numberline {4.7.4}la cr\IeC {\'e}ation d'un nouveau manager}{95}{subsection.2.4.7.4}
\contentsline {subsection}{\numberline {4.7.5}la cr\IeC {\'e}ation d'un nouvel emplacement}{95}{subsection.2.4.7.5}
\contentsline {subsection}{\numberline {4.7.6}interface d'accueil du front office}{96}{subsection.2.4.7.6}
\contentsline {subsection}{\numberline {4.7.7}Passage de la commande}{96}{subsection.2.4.7.7}
\contentsline {subsection}{\numberline {4.7.8}Gestion de la commande}{97}{subsection.2.4.7.8}
\contentsline {subsection}{\numberline {4.7.9}Gestion de la commande par le livreur}{97}{subsection.2.4.7.9}
\contentsline {subsection}{\numberline {4.7.10}Suivi de l'\IeC {\'e}tat de la commande}{98}{subsection.2.4.7.10}
\contentsline {subsection}{\numberline {4.7.11}Consulter transactions}{98}{subsection.2.4.7.11}
\contentsline {section}{\numberline {4.8}Conclusion}{99}{section.2.4.8}
\contentsline {chapter}{CONCLUSION GENERALE}{100}{chapter*.20}
\contentsline {chapter}{R\IeC {\'e}f\IeC {\'e}rences}{102}{section*.21}
